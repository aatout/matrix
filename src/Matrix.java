public class Matrix {
    public static void main(String[] args) {
        Matrix m = new Matrix();
        int matrix[][] = {{2, 1, 2}, {4, 1, 2}, {2, 3, 3}};
        int matrix1[][] = {{2, 3, 4}, {2, 4, 2}, {4, 5, 6}};

        m.additionmatrix(matrix, matrix1);
        m.scalarmatrix(matrix, 3);
        m.transposematrix(matrix);
        m.multiplymatrix(matrix, matrix1);
        m.subMatrix(matrix, 2, 2);
        m.diagonalmatrix(matrix);
        m.uppertriangle(matrix);
        m.lowertriangle(matrix);


    }

    public int[][] additionmatrix(int[][] array1, int[][] array2) {
        int data[][] = new int[array1.length][array2.length];

        for (int i = 0; i < array1.length; i++) {
            for (int j = 0; j < array2.length; j++) {
                data[i][j] = array1[i][j] + array2[i][j];
                System.out.print(data[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println("-----------------");
        return data;
    }


    public int[][] scalarmatrix(int[][] array1, int x) {
        int data[][] = new int[array1.length][array1.length];

        for (int i = 0; i < array1.length; i++) {
            for (int j = 0; j < array1.length; j++) {
                data[i][j] = array1[i][j] * x;
                System.out.print(data[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println("-----------------");
        return data;
    }

    public int[][] transposematrix(int[][] array1) {
        int data[][] = new int[array1.length][array1.length];

        for (int i = 0; i < array1.length; i++) {
            for (int j = 0; j < array1.length; j++) {
                data[i][j] = array1[j][i];
                System.out.print(data[i][j] + " ");

            }
            System.out.println();
        }
        System.out.println("-----------------");
        return data;
    }

    public int[][] multiplymatrix(int[][] array1, int[][] array2) {
        int data[][] = new int[array1.length][array2.length];

        for (int i = 0; i < array1.length; i++) {
            for (int j = 0; j < array2.length; j++) {
                data[i][j] = 0;
                for (int k = 0; k < 3; k++) {
                    data[i][j] += array1[i][k] * array2[k][j];
                }
                System.out.print(data[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println("-----------------");
        return data;
    }

    public void subMatrix(int[][] array1, int col, int row) {
        for (int i = 0; i < array1.length; i++) {
            for (int j = 0; j < array1[0].length; j++) {
                if(i==col || j==row)
                    continue;
                System.out.print(array1[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println("--------------");
    }


    public int[][] diagonalmatrix(int[][] array1) {
        int row = array1.length;
        int col = array1[0].length;
        if (row != col) {
            System.out.println("Matrix should be a square matrix");
        } else {
            for (int i = 0; i < row; i++) {
                for (int j = 0; j < col; j++) {
                    if (i < j || i > j)
                        System.out.print("0" + " ");
                    else
                        System.out.print(array1[i][j] + " ");
                }
                System.out.println();

            }
        }
        System.out.println("--------------");
        return array1;
    }


    public int[][] uppertriangle(int[][] array1) {
        int row = array1.length;
        int col = array1[0].length;
        if (row != col) {
            System.out.println("Matrix should be a square matrix");
        } else {
            for (int i = 0; i < row; i++) {
                for (int j = 0; j < col; j++) {
                    if (i < j)
                        System.out.print("0" + " ");
                    else
                        System.out.print(array1[i][j] + " ");
                }
                System.out.println();

            }
        }
        System.out.println("--------------");
        return array1;
    }


    public int[][] lowertriangle(int[][] array1) {
        int row = array1.length;
        int col = array1[0].length;
        if (row != col) {
            System.out.println("Matrix should be a square matrix");
        } else {
            for (int i = 0; i < row; i++) {
                for (int j = 0; j < col; j++) {
                    if (i > j)
                        System.out.print("0" + " ");
                    else
                        System.out.print(array1[i][j] + " ");
                }
                System.out.println();

            }
        }
        System.out.println("--------------");
        return array1;
    }
}












